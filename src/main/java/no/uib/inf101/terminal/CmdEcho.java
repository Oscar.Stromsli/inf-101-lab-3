package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String echo = "";
        for (String i : args) {
            echo += i + " ";
        }
        return echo;
    }

    @Override
    public String getName() {
        return "echo";
    }   
}